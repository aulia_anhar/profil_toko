import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  final profile = <String, String>{};

  MainApp({super.key}) {
    profile['nama'] = 'RESTAURANT';
    profile['email'] = 'xperiaanhar@gmail.com';
    profile['phone'] = '+628988989509';
    profile['image'] = 'Capture.PNG';
//Deskripsi
    profile['Deskripsi'] = 'Deskripsi';
    profile['isiDesk'] =
        'Sayap Ayam Crispy Krezz paling murah dan paling renyah. Dibuat dari sayap ayam pilihan yang masih segar membuat rasanya jadi lebih istimewa. Bisa diorder kapan saja Anda mau dengan tingkat kepedasan sambal yang bisa disesuaikan selera.';
//paket menu
    profile['menu'] = 'Paket Menu';
    profile['p1'] = '- Paket 1 harga Rp25.000';
    profile['ip1'] = """3 sayap ayam
2 nasi putih
1 es teh manis""";
    profile['p2'] = '- Paket 2 harga Rp23.000';
    profile['ip2'] = """3 sayap ayam
1 nasi putih
1 es teh manis""";
    profile['p3'] = '- Paket 2 harga Rp13.000';
    profile['ip3'] = """1 sayap ayam
1 nasi putih
1 es teh manis""";
    profile['alamat'] = 'Alamat';
    profile['jdwl'] = 'jadwal';
    profile['s-k'] = ': 09.00-20.00';
    profile['j'] = ': libur';
    profile['s-m'] = ': 13.00-23.59';
  }

  get children => null;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Aplikasi Biodata",
      home: Scaffold(
        appBar: AppBar(title: const Text("Aplikasi Profil Resto")),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(20),
          child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
//tempat kotak teks
            kotakTeks(Colors.black, profile['nama'] ?? ''),
            Image(image: AssetImage('assets/${profile["image"] ?? ''}')),
            SizedBox(
              height: 10,
            ),
//baris ikon
            Row(
              children: [
                btncontact(Icons.alternate_email, Colors.red,
                    "mailto:${profile['mail']}"),
                btncontact(Icons.mark_as_unread_rounded, Colors.green,
                    "https://wa.me/{profile['phone']}"),
                btncontact(
                    Icons.phone, Colors.blue, "tel: ${profile['phone']}"),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            kotakTeks(Colors.blueGrey, profile['Deskripsi'] ?? ''),
            SizedBox(
              height: 10,
            ),
            Text(
              profile['isiDesk'] ?? '',
              style: TextStyle(fontSize: 18),
              textAlign: TextAlign.center,
            ),

            SizedBox(
              height: 10,
            ),
            kotakTeks(Colors.blueGrey, profile['menu'] ?? ''),
            SizedBox(
              height: 5,
            ),
            kotakPaket(profile['p1'] ?? ''),
            txtAtribute(" ", profile['ip1'] ?? ''),
            kotakPaket(profile['p2'] ?? ''),
            txtAtribute(" ", profile['ip2'] ?? ''),
            kotakPaket(profile['p3'] ?? ''),
            txtAtribute(" ", profile['ip3'] ?? ''),
            SizedBox(
              height: 10,
            ),
            kotakTeks(Colors.blueGrey, profile['jdwl'] ?? ''),
            txtAtribute("Senin - kamis", profile['s-k'] ?? ''),
            txtAtribute("jumat ", profile['j'] ?? ''),
            txtAtribute("sabtu-minggu", profile['s-m'] ?? ''),
          ]),
        ),
      ),
    );
  }

  Container kotakPaket(String teks) {
    return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.all(0.5),
        width: double.infinity,
        decoration: BoxDecoration(color: const Color.fromARGB(255, 39, 39, 39)),
        child: Text(teks,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 19,
                color: Colors.white)));
  }

//setting kotak teks
  Container kotakTeks(Color bgColor, String teks) {
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      decoration: BoxDecoration(color: bgColor),
      padding: EdgeInsets.all(10),
      child: Text(
        teks,
        style: TextStyle(
            fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white),
      ),
    );
  }

//setting dalam kotak berisi teks
  Row txtAtribute(String judul, String teks) {
    return Row(
      children: [
        Container(
          width: 200,
          alignment: Alignment.center,
          child: Text("$judul",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
        ),
        Text("  ", style: TextStyle(fontSize: 18)),
        Text(teks, style: TextStyle(fontSize: 18))
      ],
    );
  }

// setting icon
  Expanded btncontact(IconData icon, var color, String uri) {
    return Expanded(
      child: ElevatedButton(
        onPressed: () {
          launch(uri);
        },
        child: Icon(icon),
        style: ElevatedButton.styleFrom(
          backgroundColor: color,
          foregroundColor: Colors.white,
          textStyle:
              TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        ),
      ),
    );
  }

  Future launch(String uri) async {
    if (!await launchUrl(Uri.parse(uri))) {
      throw Exception('tidak dapat memangil: $uri');
    }
  }
}
